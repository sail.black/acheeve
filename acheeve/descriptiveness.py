from chembee.actions.get_false_predictions import get_multi_false_predictions


def compare_descriptiveness(chembees:list): 
    """
    The compare_descriptiveness function takes in a list of ChemBees and returns a dictionary containing the false negatives, 
    false positives, number of false negatives, and number of false positives for each ChemBee. Intended 
    use is in notebooks and web applications (therefore, the dictionary data type)
    
    :param chembees:list: Used to pass in a list of bee objects.
    :return: A dictionary of dictionaries.
    
    :doc-author: Julian M. Kleber
    """
    
    result = {}
    for chembee in chembees: 
        result[bee.name]["false_neg"] = {}
        result[bee.name]["false_pos"] = {}
        result[bee.name]["false_pos_count"] = {}
        result[bee.name]["false_neg_count"] = {}
        false_neg_indices, false_pos_indices = get_multi_false_predictions(clf, X_data, y_data, n)
        result[bee.name]["false_neg"] = false_neg_indices
        result[bee.name]["false_pos"] = false_pos_indices
        result[bee.name]["false_pos_count"] = len(false_pos_indices)
        result[bee.name]["false_neg_count"] = len(false_pos_indices)

    return result
        