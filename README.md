# acheeve

The repository coordinates multiple chembees. 
A reminder: A chembee object manifests itself through 
the below shown SOLID design pattern. 

Thus, the `acheeve` object handles multiple chembees, for example to evaluate an algorithm: 

![Pattern](acheeve.png)

The above pattern, lets us implement a swarm of chembees for a wide array of endpoints fast. But we only need one API to stick them together. 

The concept is at the moment evaluated by comparing three different datasets (`chembees`). If the method proves valuable, the above pattern will be used for our `SaaS` products.

After some time and more experiments, I was totall digging into low-code automation tools. Yet, I could not find one suitable for this need. 

However, as chembees are microservices and [carate](https://codeberg.org/sail.black/carate) explored the pattern more into depth and applied it to deep learning. 

From the experience made there, it seems natural to have a tool for handling different endpoints, as the numbers can get quite large. 

The key takeaways from research about handling many endpoints are 

* Decentralized data storage, i.e. every endpoint has their own
* Running in the same cluster but in different containers/sandboxes 
    -> check out [sandstorm](https://docs.sandstorm.io/en/latest/administering/email/)
* Sufficient monitoring and orchestration is mandatory
* Hardened GPU servers, drivers can mess up stuff
* Preferrable large numbers of CPUs
